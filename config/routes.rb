# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
get 'subtasks/template', to: 'template#index'
get 'subtasks/template/new', to: 'template#new'
post 'subtasks/template/create', to: 'template#create'

match 'subtasks/template/edit/:template_id', :to => 'template#edit', :via => ['get', 'post']

post '/subtasks/template/delete/:template_id', to: 'template#delete'

