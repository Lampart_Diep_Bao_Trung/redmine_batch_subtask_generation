class ViewIssuesShowDetailsBottomHookListener < Redmine::Hook::ViewListener

    
    include TemplateHelper
    def view_issues_show_details_bottom(context = {})
        templates = get_templates()
        trackers = get_trackers()
        context[:controller].send(:render_to_string, {
            :partial => 'hooks/view_issues_show_details_bottom',
            :locals => { 
              context: context,
              templates: templates,
              trackers: trackers
            },
        })
    end
end