class ControllerIssuesEditAfterSaveHookListener < Redmine::Hook::ViewListener
    include IssueHelper
    
    # Hook function controller_issues_edit_after_save()
    def controller_issues_edit_after_save(context = {})
      raise ActiveRecord::Rollback unless updateChildIssues(context)
      true
    end
end