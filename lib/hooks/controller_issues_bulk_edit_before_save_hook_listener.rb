class ControllerIssuesBulkEditBeforeSave < Redmine::Hook::ViewListener
    include IssueHelper

    # Hook function controller_issues_bulk_edit_before_save()
    def controller_issues_bulk_edit_before_save(context = {})
      updateChildIssues(context)
      true
    end
end
