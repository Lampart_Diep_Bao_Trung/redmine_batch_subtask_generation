= redmine_batch_subtask_generation

I. Install

1. Make sure the plugin is installed to 'plugins/redmine_batch_subtask_generation'
2. Login to your Redmine install as an Administrator
3. Provide 'write' permission for 'redmine_batch_subtask_generation/template/template.json'
OR change ownership of plugin to 'redmine' user

- To provide 'write' permission for 'redmine_batch_subtask_generation/template/template.json' :
"chmod 777 /path/to/redmine_batch_subtask_generation/template/template.json"

- To change ownership of plugin to 'redmine' user:
"chown -R redmine:redmine /path/to/redmine_batch_subtask_generation" 

II. Usage

1. Subtasks template  
- To be able to create subtask template, you must have the 'administrator' permission.
- The 'Subtasks template' will appear when click 'administrator' at menu bar
2. Generate subtasks 
- The 'Generate' button will appear when you click to see issue detail