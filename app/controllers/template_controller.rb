class TemplateController < ApplicationController

  before_action :require_admin

  include TemplateHelper

  def index
    @templates = get_templates()
    @trackers = get_trackers()
  end

  def new 
    @trackers = get_trackers()
  end

  def create 
    error = validate_create()
    unless error.empty?
       respond_to do |format|
          format.html { 
            redirect_to action: 'new'
            flash[:error] = error 
          }
        end
        return;
    end

    new_template = generate_template()
    template_file_contents = get_templates()
    template_file_contents << new_template
    write_json(template_file_contents)

    respond_to do |format|
      format.html { 
        redirect_to action: 'index'
        flash[:notice] = 'Successful creation.' 
      }
    end
  end

  def edit
    if request.method == 'GET'
      # GET
      template_id = params[:template_id]
      @template = get_template_by_id(template_id)
      @trackers = get_trackers()

    else
      # POST
      error = validate_edit()
      unless error.empty?
        respond_to do |format|
            format.html { 
              redirect_to action: 'edit'
              flash[:error] = error 
            }
        end
        return;
      end

      template_id = params[:template_id]
      template_file_contents = get_templates()
      unless template_file_contents.empty?
        template_file_contents.each_with_index  do |template, idx| 
          if template['id'].to_s == template_id.to_s
            template_file_contents[idx] = generate_template()
            break
          end
        end
        write_json(template_file_contents)
        # success
        respond_to do |format|
          format.html { 
            redirect_to action: 'index'
            flash[:notice] = 'Successful update.' 
          }
        end
      else 
        # failed
        respond_to do |format|
          format.html { 
            redirect_to action: 'index'
            flash[:error] = 'Update failed!' 
          }
        end
      end
    end
  end

  def delete
    template_id = params[:template_id]
    template_file_contents = get_templates()

    unless template_file_contents.empty?
      template_file_contents.each_with_index  do |template, idx| 
        if template['id'].to_s == template_id.to_s
          template_file_contents.delete_at(idx)
          break
        end
      end
      write_json(template_file_contents)
    end

    # Success
    respond_to do |format|
      format.html { 
        redirect_to action: 'index'
        flash[:notice] = 'Successful delete.' 
      }
    end
  end

end
