

function SubtaskTemplate (options) {
    var self = this;

    this.btnAddTemplate = `<button type="button" class="add-template">+</button>`;
    this.btnDeleteTemplate = `<button type="button" class="delete-template">X</button>`;
    this.selectTrackerEle = '';
    this.ulEle = `<ul class="list"></ul>`;
    this.liEl = '';
    this.init = function () {
        let trackers = options.hasOwnProperty('trackers') ?  options.trackers : {};
        this.setTrackerEle(trackers);
        
        this.liEle = `<li class="item">${this.selectTrackerEle}${this.btnAddTemplate}${this.btnDeleteTemplate}${this.ulEle}</li>`;
    }

    this.setTrackerEle = function (trackers) {
        let option = '<option value=""></option>'
        $.each(trackers, function(id, name) {
            option += `<option value="${id}">${name}</option>`;
        });
        this.selectTrackerEle = `<select>${option}</select>`
    }

    this.addTemplate = function (e) {
        e.preventDefault();
        let $liEle = $(self.liEle);
        if ($(this).parent().hasClass('item')) {
            $(this).parent().find('> ul').append($liEle);
        } else {
            $(this).parent().find('.tree > ul').append($liEle);
        }
        self.bindEventBtnChild($liEle);
    }


    this.deleteTemplate = function (e) {
        e.preventDefault();
        $(this).parent().remove();
    }

    this.bindEventBtnChild = function ($parent) {
        $parent.find('.add-template').on('click', this.addTemplate);
        $parent.find('.delete-template').on('click', this.deleteTemplate);
        // $parent.find('> select').on('change', this.validateSelectOption);
    }

    this.addCsrfToken = function ($formEle) {
        let csrfToken = $('meta[name="csrf-token"]').prop('content');
        if ($formEle.find('[name="authenticity_token"]').length > 0) {
            $formEle.find('[name="authenticity_token"]').val(csrfToken);
        } else {
            let $auth_input = $(`<input type="hidden" name="authenticity_token" value="${csrfToken}">`);
            $formEle.append($auth_input);
        }
    }

    this.renderTemplatesAsEdit = function (templates, $ulEle) {
        for (let i in templates) {
            let template = templates[i];
            let $liEle = $(self.liEle);
            $ulEle.append($liEle);
            $liEle.find('select').val(template['tracker_id']);
            this.bindEventBtnChild($liEle);

            if (template['children'] != undefined && template['children'].length > 0) {
                this.renderTemplatesAsEdit(template['children'], $liEle.find('> ul'));
            }
        }
    }

    this.createJsonTemplateFromLayout = function ($liEles) {
        let templates = []
        $.each($liEles, function () {
            let template = {}
            let tracker_id = parseInt($(this).find('> select').val());
            if (tracker_id) {
                template['tracker_id'] = tracker_id
                if ($(this).find('> ul li').length > 0) {
                    template['children'] = self.createJsonTemplateFromLayout($(this).find('> ul > li'));
                }
                templates.push(template);
            }
        });
        return templates;
    }

    this.updateTemplateStructure = function ($formEle) {
        let templates = this.createJsonTemplateFromLayout($('.subtask-template ul.list.wrapper > li'));
        $formEle.find('[name="template[structure]"]').val(JSON.stringify(templates));
    }


    this.validateSelectOption = function () {
        let listValueSelected = [];

        let $selectEles = $(this).closest('ul').find('> li > select');

        $selectEles.each(function() {
            if ($(this).val()) {
                listValueSelected.push($(this).val());
            }
        });


        $selectEles.find('option').prop('disabled', false);
        $.each(listValueSelected, function (idx, val) {
            $selectEles.find('option[value="'+ val +'"]').prop('disabled', true);
        });
    }
    
    this.init();

}
