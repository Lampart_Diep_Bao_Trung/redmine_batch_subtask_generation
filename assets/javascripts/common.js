
function crawApiKey (html) {
    return $(html).find('div.box pre').text();
}

function setCookie(key, value) {
    document.cookie = key + '=' + value + '; max-age=86400; path=/;';
    return true;
}

function getCookie(key) {
    let cookie = document.cookie
        .split('; ')
        .find(row => row.startsWith(key + '='));
    if (cookie) {
        return cookie.split('=')[1];
    }
    return null;
}

function formatDate (date) {
    let d = new Date(date);
    return d.getFullYear()+ "-" + ("0"+(d.getMonth()+1)).slice(-2) + '-' + ("0" + d.getDate()).slice(-2);
}

function initModal (modalId) {
    // Get the modal
    $modal = $(modalId);
    var closeBtn = $modal.find(".close");
    closeBtn.on('click', function (e) {
        closeModal(modalId);
    });

    var cancelBtn = $modal.find(".cancel");
    cancelBtn.on('click', function (e) {
        closeModal(modalId);
    });

    var confirmlBtn = $modal.find(".confirm");
    confirmlBtn.on('click', function (e) {
        closeModal(modalId);
    });

    $modal.find('.modal-content').on('click', function (e) {
        e.stopPropagation();
    });

    $('#select_template_modal').on('click', function () {
        closeModal(modalId);
    });
}

function showModal (modalId) {
    $(modalId).css({display:"block"})
}

function closeModal (modalId) {
    $(modalId).css({display:"none"})
}