function ajaxSend(type, url, data) {
  data = data != undefined ? data : {};
  return getRedmineApiKey().then((redmineApiKey) => {
    return new Promise(function (resolve, reject) {
      $.ajax({
        type: type,
        url: url,
        headers: {
          "X-Redmine-API-Key": redmineApiKey,
        },
        contentType: "application/json",
        data: data,
        success: function (data) {
          resolve(data);
        },
        error: function (e) {
          reject(e);
        },
      });
    });
  });
}

/**
 * Call redmine API to create issue
 *
 * @param {*} issue
 * @param {*} parentIssueId
 * @returns Promise
 */
function apiCreateIssue(issue) {
  // Get redmine API key
  let data = JSON.stringify(issue);
  return ajaxSend("POST", URL.ISSUES, data);
}

function apiDeleteIssue(issueId) {
  let url = URL.DELETE_ISSUE.replace("{issueId}", issueId);
  return ajaxSend("DELETE", url);
}

/**
 *
 * @param {*} issueId
 * @returns
 */
function apiGetIssueAndChildrens(issueId) {
  let url = URL.ISSUE.replace("{issueId}", issueId) + "?include=children";
  return ajaxSend("GET", url);
}

/**
 *
 * @param {*} issueId
 * @returns
 */
function apiGetIssue(issueId) {
  let url = URL.ISSUE.replace("{issueId}", issueId);
  return ajaxSend("GET", url);
}

/**
 * Get redmine API key
 *
 * @returns Promise
 */
function getRedmineApiKey() {
  return new Promise(function (resolve, reject) {
    let redmineApiKey = getCookie("redmine_api_key");
    if (redmineApiKey) {
      return resolve(redmineApiKey);
    }
    $.ajax({
      type: "GET",
      url: URL.API_KEY,
      contentType: "text/javascript charset=utf-8",
      success: function (html) {
        let api_key = crawApiKey(html);
        setCookie("redmine_api_key", api_key);
        resolve(api_key);
      },
      error: function (e) {
        reject(e);
      },
    });
  });
}
